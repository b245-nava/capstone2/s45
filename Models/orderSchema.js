const mongoose = require('mongoose');

// Cart function
const orderSchema = new mongoose.Schema({
	transactionStart: {
		type: Date,
		default: new Date
	},
	purchaseDate: {
		type: Date,
		default: new Date
	},
	userId: {
		type: String,
		required: [true, "Please include purchasing user details."]
	},
	ingredients: [
		{
			productId: {
				type: String,
				required: [true, "productId is required."]
			},
			productName: {
				type: String
			},
			quantity: {
				type: Number,
				default: 1
			},
			subTotal: {
				type: Number,
				default: 0
			}
		}],
	address: {
		type: String
	},
	instructions: {
		type: String,
		default: "No special instructions"
	},
	status: {
		type: String,
		default: "Pending"
	},
	total: {
		type: Number,
		default: 0
	}
});

module.exports = mongoose.model("Order", orderSchema);