const mongoose = require('mongoose');

const cartSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "Please specify user associated to cart."]
	},
	products: [
	{
		productId: {
			type: String
		},
		quantity: {
			type: Number,
			default: 1
		}
	}
	],
	address: {
		type: String
	},
	total: {
		type: Number,
		default: 0
	}
});



module.exports = mongoose.model("Cart", cartSchema);