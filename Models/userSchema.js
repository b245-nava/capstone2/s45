const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Please provide a valid email address."]
	},
	password: {
		type: String,
		required: [true, "Please create a strong password."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	}
});

module.exports = mongoose.model("User", userSchema);