const express = require('express');
const router = express.Router();
const prodController = require('../Controllers/prodControllers.js');
const auth = require('../auth.js');

// Route for viewing all available products
router.get('/menu', prodController.menu);

// Route for viewing all products
router.get('/allMenu', auth.verify, prodController.allMenu);

// Route for viewing specific product types
router.get('/menu/wok', prodController.menuWok);
router.get('/menu/toppings', prodController.menuTops);
router.get('/menu/sauce', prodController.menuSauce);

// Route for viewing ALL products by category (Admin)
router.get('/menu/allWok', auth.verify, prodController.allWok);
router.get('/menu/allTops', auth.verify, prodController.allTops);
router.get('/menu/allSauce', auth.verify, prodController.allSauce);

// Route to search a product by name
router.post('/search', prodController.searchOne);

// Route for seeing a specific product
router.get('/:menuId', prodController.menuOne);

// Route for product creation
router.post('/create', auth.verify, prodController.prodCreate);

// Route for product updating
router.put('/edit/:menuId', auth.verify, prodController.prodEdit);

// Route for archiving a product/updating availability
router.put('/archive/:menuId', auth.verify, prodController.prodArchive);




module.exports = router;