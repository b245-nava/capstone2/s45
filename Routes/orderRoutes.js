const express = require('express');
const router = express.Router();
const orderController = require('../Controllers/orderControllers.js');
const cartController = require('../Controllers/cartControllers.js');
const auth = require('../auth.js');

// [Order Routes]

// Add ingredient to cart w/ name
router.post('/add', auth.verify, orderController.add);

// Create Order
router.post("/:prodId", auth.verify, orderController.newOrder);

// Add ingredient to cart w/ prodId
router.post('/addToCart/:prodId', auth.verify, orderController.addToCart);

// Clearing cart
router.put('/clear', auth.verify, orderController.clear);

// Deleting an item from cart
router.put('/delete/:prodId', auth.verify, orderController.delete);

// Checking out cart/order, updating status
router.put('/checkout', auth.verify, orderController.checkout);

// router.post('/checkout', auth.verify, orderController.checkout);

// Retrieve Own Orders (Authenticated User)
router.get('/myOrders', auth.verify, orderController.myOrders);

// Retrieve All Orders (Admin Only)
router.get('/allOrders', auth.verify, orderController.allOrders);

// Retrieve All Orders in Kitchen (Admin Only)
router.get('/kitchenOrders', auth.verify, orderController.kitchenOrders);

// Retrieve All Completed Orders (Admin Only)
router.get('/completedOrders', auth.verify, orderController.completedOrders);

// Update to En Route (Admin Only)
router.put('/update1/:userId', auth.verify, orderController.enRoute);

// Update to Completed (Admin Only)
router.put('/update2/:userId', auth.verify, orderController.completed);

// [Cart Routes]

// Viewing own cart (Authenticated User)
router.get('/myCart', auth.verify, cartController.myCart);

// Starting new cart by adding a product
router.post('/startCart/:prodId', auth.verify, cartController.newCart);

// Adding a product to the cart
// router.post('/addToCart/:prodId', auth.verify, cartController.addToCart);

module.exports = router;