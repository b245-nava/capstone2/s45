const mongoose = require('mongoose');
const Order = require('../Models/orderSchema.js');
const Product = require('../Models/prodSchema.js');
const User = require('../Models/userSchema.js');
const auth = require('../auth.js');

// Creating new order
/*
	Business Logic:
		1. Only users w/o admin access can create orders.
		2. An order must contain the ff:
			- userId of registered customer making order
			- list of ordered products (array of objects: indicate product ID and quantity of item)
			- user's address
			- total of order

		This means that:
			- UserId must be pushed onto order's records, while the order's ID can be associated with the user that made it
			- The chosen product's ID needs to be pushed onto the order form, and quantity needs to be indicated
			- The user needs to indicate the address to which their order will be delivered.

	Possible user-end errors:
		1. User is an admin
		2. Invalid product ID

	Possible code bugs:
		1. Non-existent product ID gets added into order anyway
		2. totalAmount does not automatically and precisely reflect the total price of the order
*/

// Beginning of new order
module.exports.newOrder = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const prodId = request.params.prodId;
	const input = request.body;

	if(userData.isAdmin){
		return response.send(false)
	} else {
	// Algo to find orders w/ userId + pending stat
		Order.findOne({
			userId: userData._id,
			status: "Pending"
		})
		.then(order => {
		// If pending order is not found, start new order
			if(order === null){

				Product.findById({_id: prodId})
				.then(item => {
				// In case of incorrect product ID
					if(item === null){
						return response.send(false);
						// return false
					}

					let prod = item;
				// New order initiated
					let newOrder = new Order({
						userId: userData._id,
						ingredients: [
							{
								productId: prodId,
								productName: prod.name,
								quantity: input.quantity,
								subTotal: item.price*input.quantity
							}
						],
						total: item.price*input.quantity
					});
					// console.log('New Order')
					newOrder.save()
					.then(save => {
						// console.log(save);
						return response.send(save)
					})
					.catch(err => {
						// console.log(err);
						return response.send(false)
						// return false;
					})
				})
				.catch(err => {
					// console.log(err);
					// return false;
					return response.send(false)
				})


			} else {
				
				// Add to Cart function

				const input = request.body;

				Product.findById({_id: prodId})
				.then(item => {

					if(!item){
						// console.log('no item')
						return response.send(false);
						// return false;
					} else {

						let prod = item;

						let subTotal = item.price*input.quantity;
						order.total = order.total + subTotal;
						order.ingredients.push({
							productId: prodId,
							productName: prod.name,
							quantity: input.quantity,
							subTotal: item.price*input.quantity
						});

						order.save()
						.then(saved => {
							// console.log(saved);
							return response.send(saved);
							// return true;
						})
						.catch(err => {
							// console.log(err);
							return response.send(false);
							// return false;
						})
					}
				})
				.catch(err => {
					// console.log(err);
					return response.send(false);
					// return false;
				})

			}
		})
		.catch(err => {
			// console.log(err);
			return response.send(false);
		})
	}
		/*{
		Product.findById({_id: prodId})
		.then(result => {

			if(result === null){
				return response.send('Please double-check the product ID and try again.')
			} else {

				let newCart = new Cart({
					userId: userData._id,
					products: [
						{
							productId: result._id,
							quantity: input.quantity,
							address: input.address
						}
					],
					total: result.price*input.quantity
				});

				newCart.save()
				.then(result => {
					// console.log(result);
					return response.send(`Your order has been added to your cart! Here is your cart ID: ${result._id}.`)})
				.catch(err => {
					// console.log(err);
					return response.send('Sorry, there was an error in saving your cart. Please try again.')
				});
			}
		})
		.catch(err => {
			// console.log(err);
			return response.send('Sorry, there was an error validating the product ID. Please try again!');
		});
	}*/
};

// Add to cart using prodId params
module.exports.addToCart = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const prodId = request.params.prodId;

	if(userData.isAdmin){
		return response.send(false);
		// return false;
	}

	// Find pending order
	Order.findOne({
		userId: userData._id,
		status: "Pending"
	})
	.then(order => {

		if(!order){
			return response.send(false)
			// newOrder(request,response);
		} else {

			const input = request.body;

			Product.findById({_id: prodId})
			.then(item => {

				if(!item){
					return response.send(false);
					// return false;
				} else {

					let subTotal = item.price*input.quantity;
					order.total = order.total + subTotal;
					order.ingredients.push({
						productId: prodId,
						quantity: input.quantity,
						subTotal: item.price*input.quantity
					});

					order.save()
					.then(saved => {
						// console.log(saved);
						return response.send(true);
						// return true;
					})
					.catch(err => {
						// console.log(err);
						return response.send(false);
						// return false;
					})
				}
			})
			.catch(err => {
				// console.log(err);
				return response.send(false);
				// return false;
			})
		}

	})
	.catch(err =>{
		// console.log(err);
		return response.send(false);
		// return false;
	})
};

// Add to cart by inputting product name
module.exports.add = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	// const prodId = request.params.prodId;

	if(userData.isAdmin){
		return response.send(false);
		// return false;
	}

	// Find pending order
	Order.findOne({
		userId: userData._id,
		status: "Pending"
	})
	.then(order => {

		if(!order){
			return response.send(false)
			// newOrder(request,response);
		} else {

			const input = request.body;

			Product.findOne({name: {$regex: input.name, $options: "$i"}})
			.then(item => {
				// console.log(item);
				if(!item){
					return response.send(false);
					// return false;
				} else {

					let subTotal = item.price*input.quantity;
					order.total = order.total + subTotal;
					order.ingredients.push({
						productId: item._id,
						quantity: input.quantity,
						subTotal: item.price*input.quantity
					});

					order.save()
					.then(saved => {
						// console.log(saved);
						return response.send(true);
						// return true;
					})
					.catch(err => {
						// console.log(err);
						return response.send(false);
						// return false;
					})
				}
			})
			.catch(err => {
				// console.log(err);
				return response.send(false);
				// return false;
			})
		}

	})
	.catch(err =>{
		// console.log(err);
		return response.send(false);
		// return false;
	})
};

// Deleting single item from cart
module.exports.delete = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const prodRemove = request.params.prodId;

	if(userData.isAdmin){
		return response.send(false);
		// return false;
	} else {

		Order.findOne({userId: userData, status: "Pending"})
		.then(order =>{

			if(!order){
				return response.send(false);
				// return false;
			} else {

				let prodInd = null;
				let ordItem = order.ingredients.find((item, i) => {

					if(item.productId === prodRemove){
						prodInd = i;
						return response.send(true);
					} 
					return response.send(false);
				})

				order.total = order.total - ordItem.subTotal;
				order.ingredients.splice(prodInd, 1);

				order.save()
				.then(save => {
					// console.log(save);
					return response.send(true);
					// return true;
				})
				.catch(err => {
					// console.log(err);
					return response.send(false);
					// return false;
				})
			}
		})
		.catch(err => {
			// console.log(err);
			return response.send(false);
			// return false;
		})
	}
};

// Clearing cart
module.exports.clear = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		return response.send(false);
	} else {

		Order.findOne({
			userId: userData._id,
			status: "Pending"
		})

		.then(result => {

			if(!result){
				return response.send(false);
			} else {

				order.ingredients = [];
				order.total = 0;

				order.save()
				.then(res => response.send(res))
				.catch(err => {
					// console.log(err);
					return response.send(false);
				})
			}
		})
		.catch(err => {
			// console.log(err);
			return response.send(false);
		})
	}
};

// Cart/Order checkout
module.exports.checkout = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const input = request.body
	
	if(userData.isAdmin){
		return response.send(false);
		// return false;
	} else {

		Order.findOne({userId: userData._id, status: "Pending"})
		.then(order => {

			if(!order){
				return response.send(false);
				// return false;
			} else {

				// No items in order/cart
				if(order.ingredients.length === 0){
					return response.send(false);
					// return false;
				} else {

					// Possible statuses: Pending, In Kitchen, On the way, Completed
					order.status = "In Kitchen";
					order.address = input.address;
					order.instructions = input.instructions;
					order.purchaseDate = new Date();
					order.save()
					.then(save => {
						// console.log(save);
						return response.send(save);
						// return true;
					})
				}
			}
		})
		.catch(err => {
			// console.log(err);
			return response.send(false);
			// return false;
		})
	}
};

/*module.exports.checkout = (request, response) => {

	const userData = auth.decode(request.headers.authorization); // to verify if user is admin and to extract userId
	const input = request.body; // product IDs, quantity, and address go here (follow orderSchema)

	if(userData.isAdmin){
		return false;
	} else {

		let newOrder = new Order({
			userId: userData._id,
			noodle: input.noodle,
			topping1: input.topping1,
			topping2: input.topping2,
			topping3: input.topping3,
			sauce: input.sauce,
			address: input.address,
			quantity: input.quantity,
			instructions: input.instructions
		});

		let totalAmount = newOrder.totalAmount;

		// Verifying noodle ID and price
		Product.findOne({name: input.noodle})
		.then(noodle => {

			if(noodle === null){
				return false
			} else {

				let noodlePrice = noodle.price;

				totalAmount = totalAmount + noodlePrice;

				// Verifying topping 1's ID and price
				Product.findOne({name: input.topping1})
				.then(topping => {

					if(topping === null){
						return false
					} else {

						let toppingPrice = topping.price;

						totalAmount = totalAmount + toppingPrice;

						// Verifying topping 2
						Product.findOne({name: input.topping2})
						.then(topping2 => {

							if(topping2 === null){
								return false;
							} else {

								totalAmount = totalAmount + topping2.price;

								// Verifying topping 3
								Product.findById(input.topping3)
								.then(topping3 => {

									if(topping3 === null){
										return false;
									} else {

										// If user wants 3 toppings and has selected a regular size wok, the size will automatically be upgraded to large.
										if(topping.price>0 && topping2.price>0 && topping3.price>0 && noodlePrice === 120){

											Product.findById('63e1a18dff5e2f4aec490d51')
											.then(large => {

												if(!large.isAvailable){
													return false
												} else {

													totalAmount = totalAmount + topping3.price + (large.price - 120);

													// Verifying sauce ID and price
													Product.findById(input.sauce)
													.then(sauce => {

														if(sauce === null){
															return false
														} else {

															newOrder.totalAmount = totalAmount + sauce.price;

															newOrder.totalAmount = newOrder.totalAmount*input.quantity;

															// All product IDs and prices should have been verified by this point
															newOrder.save()
															.then(result => {
																// console.log(result);
																// return response.send(`Thank you for placing your order with us! Your receipt number is ${result._id} and your total is ${result.totalAmount}. (Please be informed that your wok size has been upgraded to Large since you have selected three toppings, hence the additional charge of 40.)`)
																return true															
																// Assuming the code flow is correct, result.totalAmount should reflect the total of the noodles, toppings, and sauce.
															})
															.catch(err => {
																// console.log(err);
																return false
															});
														}
													})
													.catch(err => {
														// console.log(err);
														return false;
													})
												}
											})
											.catch(err => {
												// console.log(err);
												return false;
											})

										} else {

											totalAmount = totalAmount + topping3.price;

											// Verifying sauce ID and price
											Product.findById(input.sauce)
											.then(sauce => {

												if(sauce === null){
													return false
												} else {

													newOrder.totalAmount = totalAmount + sauce.price;

													newOrder.totalAmount = newOrder.totalAmount*input.quantity;

													// All product IDs and prices should have been verified by this point
													newOrder.save()
													.then(result => {
														// console.log(result);
														// return response.send(`Thank you for placing your order with us! Your receipt number is ${result._id} and your total is ${result.totalAmount}.`)
														return true														
														// Assuming the code flow is correct, result.totalAmount should reflect the total of the noodles, toppings, and sauce.
													})
													.catch(err => {
														// console.log(err);
														return false
													});
												}
											})
											.catch(err => {
												// console.log(err);
												return false;
											})
										}
									}
								})
								.catch(err => {
									// console.log(err);
									return false;
								})
							}
						})
						.catch(err => {
							// console.log(err);
							return false;
						})
					}
				})
				.catch(err => {
					// console.log(err);
					return false;
				})
			}
		})
		.catch(err => {
			// console.log(err);
			return false;
		});
	}
};*/

// Retrieving own orders
module.exports.myOrders = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	Order.find({userId: userData._id, status: "Pending"})
	.then(res => response.send(res))
	.catch(err => {
		// console.log(err);
		return response.send(false);
	});
};

// Retrieving all orders (Admin Only)
module.exports.allOrders = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){
		// return response.send('Sorry, you are not authorized to do this. Looking for your order history? Log in and head to /myOrders!');
		return response.send(false)
	} else {
		Order.find()
		.then(res => response.send(res))
		.catch(err => {
			// console.log(err);
			return response.send(false);
		});
	}
};

// Retrieving all orders in kitchen (Admin Only)
module.exports.kitchenOrders = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){
		// return response.send('Sorry, you are not authorized to do this. Looking for your order history? Log in and head to /myOrders!');
		return response.send(false)
	} else {
		Order.find({status: "In Kitchen"})
		.then(res => response.send(res))
		.catch(err => {
			// console.log(err);
			return response.send(false);
		});
	}
};

// Retrieving all completed orders (Admin Only)

module.exports.completedOrders = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){
		// return response.send('Sorry, you are not authorized to do this. Looking for your order history? Log in and head to /myOrders!');
		return response.send(false)
	} else {
		Order.find({status: "Completed"})
		.then(res => response.send(res))
		.catch(err => {
			// console.log(err);
			return response.send(false);
		});
	}
};

// Updating a User's Order Status to En Route (Admin Only)
module.exports.enRoute = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const user = request.params.userId;

	if(!userData.isAdmin){
		return response.send(false);
	} else {

		Order.findOne({
			userId: user,
			status: "In Kitchen"
		})
		.then(result => {

			if(!result){
				// console.log(result);
				return response.send(false);
			} else {
				// console.log(result);
				result.status = "On the way";

				result.save()
				.then(save => {
					// console.log(save);
					return response.send(save);
				})
				.catch(err => {
					// console.log(err);
					return response.send(false);
				})
			}
		})

	}
};

// Updating to Completed (Admin)
module.exports.completed = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const user = request.params.userId;

	if(!userData.isAdmin){
		return response.send(false);
	} else {

		Order.findOne({
			userId: user,
			status: "On the way"
		})
		.then(result => {

			if(!result){
				return response.send(false);
			} else {

				result.status = "Completed";

				result.save()
				.then(save => {
					// console.log(save);
					return response.send(save);
				})
				.catch(err => {
					// console.log(err);
					return response.send(false);
				})
			}
		})

	}
};