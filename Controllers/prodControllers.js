const mongoose = require('mongoose');
const Product = require('../Models/prodSchema.js');
const auth = require('../auth.js');

// Product Creation
module.exports.prodCreate = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const input = request.body;

	if(!userData.isAdmin){
		return response.send(false);
	} else {

		Product.findOne({name: input.name})
		.then(result => {

			if(result !== null){
				return response.send(false);
			} else {

				let newProd = new Product({
					name: input.name,
					description: input.description,
					price: input.price,
					itemClass: input.itemClass,
					image: input.image
				})

				newProd.save()
				.then(result => {
					// Product has been created
					return response.send(true)
				})
				.catch(err => {
					// console.log(err)
					return response.send(false)
				})
			}
		})
		.catch(err => response.send(false))
	}
};

// View Menu (Only available products)
module.exports.menu = (request, response) => {
	Product.find({isAvailable: true})
	.then(result => response.send(result))
	.catch(err => {
		// console.log(err);
		return response.send(false)
	})
};

// View Menu (All products)
module.exports.allMenu = (request, response) => {
	
	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){
		return response.send(false);
	} else {

		Product.find()
		.then(result => response.send(result))
		.catch(err => {
			// console.log(err);
			return response.send(false)
		})
	}
};

// View Menu (Available Wok)
module.exports.menuWok = (request, response) => {
	Product.find({itemClass: "wok", isAvailable: true})
	.then(result => response.send(result))
	.catch(err => {
		// console.log(err);
		return response.send(false)
	})
};

// View Menu (Available Toppings)
module.exports.menuTops = (request, response) => {
	Product.find({itemClass: "toppings", isAvailable: true})
	.then(result => response.send(result))
	.catch(err => {
		// console.log(err);
		return response.send(false)
	})
};

// View Menu (Available Sauces)
module.exports.menuSauce = (request, response) => {
	Product.find({itemClass: "sauce", isAvailable: true})
	.then(result => response.send(result))
	.catch(err => {
		// console.log(err);
		return response.send(false)
	})
};

// View by Categories (Admin)
module.exports.allWok = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){
		return response.send(false);
	} else {

		Product.find({itemClass: "wok"})
		.then(result => response.send(result))
		.catch(err => {
			// console.log(err);
			return response.send(false)
		})
	}
};

module.exports.allTops = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){
		return response.send(false);
	} else {

		Product.find({itemClass: "toppings"})
		.then(result => response.send(result))
		.catch(err => {
			// console.log(err);
			return response.send(false)
		})
	}
};

module.exports.allSauce = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){
		return response.send(false);
	} else {
		
		Product.find({itemClass: "sauce"})
		.then(result => response.send(result))
		.catch(err => {
			// console.log(err);
			return response.send(false)
		})
	}
};

// Retrieve a single product
module.exports.menuOne = (request, response) => {

	const menuId = request.params.menuId;

	Product.findById(menuId)
	.then(result => response.send(result))
	.catch(err => {
		// console.log(err)
		return response.send(false)})

};

// Retrieve a single product by name
module.exports.searchOne = (request, response) => {

	const input = request.body;

	Product.findByOne({name: {$regex: input.name, $options: '$i'}})
	.then(item => {
		// console.log(item);
		if(!item){
			return response.send('false');
		} else {
			return response.send(item);
		}
	})
	// .catch(err => console.log(err));
};

// Edit a specified product
module.exports.prodEdit = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const menuId = request.params.menuId;
	const input = request.body;

	if(!userData.isAdmin){
		return response.send(false)
	} else {

		Product.findById({_id: menuId})
		.then(result => {

			if(result === null){
				return response.send(false)
			} else {

				let menuUpdate = {
					name: input.name,
					description: input.description,
					price: input.price,
					itemClass: input.itemClass
				};

				Product.findByIdAndUpdate(menuId, menuUpdate, {new: true})
				.then(result => response.send(true))
				.catch(err => {
					// console.log(err);
					return response.send(false)
				})
			}
		})
		.catch(err => {
			// console.log(err);
			return response.send(false)
		});
	}
};

// Archive a specified product
module.exports.prodArchive = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const menuId = request.params.menuId;
	// const input = request.body;

	if(!userData.isAdmin){
		return response.send(false);
	} else {

		Product.findById({_id: menuId})
		.then(result => {

			if(result === null){
				return response.send(false);
			} else {

				if(result.isAvailable){

					let unavail = {isAvailable: false}

					Product.findByIdAndUpdate(menuId, unavail, {new: true})
					.then(result => response.send(true))
					.catch(err => {
						// console.log(err);
						return response.send(false)
					});
				} else {

					let avail = {isAvailable: true}

					Product.findByIdAndUpdate(menuId, avail, {new: true})
					.then(result => response.send(true))
					.catch(err => {
						// console.log(err);
						return response.send(false)
					});

				}
			}
		})
		.catch(err => {
			// console.log(err);
			return response.send(false)
		});
	}
};