const mongoose = require('mongoose');
const User = require('../Models/userSchema.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// User Registration
module.exports.userRegistration = (request, response) => {

	const input = request.body;

	User.findOne({email: input.email})
	.then(result => {

		if(result !== null){
			return response.send(false)
		} else {

			let newUser = new User({
				email: input.email,
				password: bcrypt.hashSync(input.password, 10)
			});
			newUser.save()
			.then(save => response.send(true))
			.catch(err => {
				// console.log(err);
				return response.send(false)
			});
		}
	})
	// .catch(err => {response.send(err)})
};

// User Login/Authentication
module.exports.userAuth = (request, response) => {

	const input = request.body;

	User.findOne({email: input.email})
	.then(result => {

		if(result === null){
			return false
		} else {

			const passChecker = bcrypt.compareSync(input.password, result.password);

			if(passChecker){
				return response.send({auth: auth.createAccessToken(result)});
			} else {
				return response.send(false);
			}
		}
	})
	// .catch(err => response.send(err));
};

// (Own) User Detail Retrieval
module.exports.myProfile = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	User.findById(userData._id)
	.then(result => {
		result.password = "*******";
		return response.send(result);
	})
	.catch(err => {
		// console.log(err);
		return response.send(false)
	});
};

// Granting/removing users admin access
module.exports.giveAdmin = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const candidateId = request.params.userId;
	const input = request.body;

	if(!userData.isAdmin){
		return response.send(false)
	} else {

		User.findById({_id: candidateId})
		.then(result => {

			if(result === null){
				return response.send(false)
			} else {

				let promotion = {
					isAdmin: input.isAdmin
				};

				User.findByIdAndUpdate(candidateId, promotion, {new: true})
				.then(result => response.send(true))
				.catch(err => {
					// console.log(err);
					return response.send(false)
				})
			}
		})
		.catch(err => {
			// console.log(err);
			return response.send(false)
		});
	}
};