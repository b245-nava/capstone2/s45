const mongoose = require('mongoose');
const Cart = require('../Models/cartSchema.js');
const User = require('../Models/userSchema.js');
const Product = require('../Models/prodSchema.js')
const auth = require('../auth.js');

// Cart
/*
	Business Logic:
		- What is a "cart" for? It's for:
			- Holding and storing the user's chosen products
			- The user can reach in and add products to the cart, or remove products as they please
		- Since admins can't check out products, they shouldn't be able to use a cart either
		- A cart is automatically created for users when they select products to buy, and a user can have multiple carts
		- The cart is "returned" once the user checks out their products at the cashier
*/

// Controller for starting a cart (Users only, users with no carts started yet only)
module.exports.newCart = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const prodId = request.params.prodId;
	const input = request.body;

	if(userData.isAdmin){
		return response.send('Please log into your user account to make purchases!')
	} else {
		Product.findById({_id: prodId})
		.then(result => {

			if(result === null){
				return response.send('Please double-check the product ID and try again.')
			} else {

				let newCart = new Cart({
					userId: userData._id,
					products: [
						{
							productId: result._id,
							quantity: input.quantity,
							address: input.address
						}
					],
					total: result.price*input.quantity
				});

				newCart.save()
				.then(result => {
					// console.log(result);
					return response.send(`Your order has been added to your cart! Here is your cart ID: ${result._id}.`)})
				.catch(err => {
					// console.log(err);
					return response.send('Sorry, there was an error in saving your cart. Please try again.')
				});
			}
		})
		.catch(err => {
			// console.log(err);
			return response.send('Sorry, there was an error validating the product ID. Please try again!');
		});
	}
};


// Controller to view own cart (Authenticated User)
module.exports.myCart = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		return response.send('Please log into your user account to make and view a cart!');
	} else {

		Cart.find({userId: userData._id})
		.then(res => response.send(res))
		.catch(err => {
			// console.log(err);
			return response.send('Sorry, there was an error retrieving your cart. Please try again!');
		})
	}
}

// Adding to cart
module.exports.addToCart = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const input = request.body;
	const prodId = input.productId;
	const prodQuantity = input.quantity;

	if(userData.isAdmin){
		return response.send('Please log into your user account to make purchases!')
	} else {

		Cart.find({userId: userData._id})
		.then(userCart => {

			console.log(userCart);

			Product.findById({_id: prodId})
			.then(foundProduct => {

				if(foundProduct === null){
					return response.send('Invalid product ID. Please double-check and try again!');
				} else {

					// Add product's price * quantity to cart's original total (which is the price of the product*quantity)
					// "userCart.total" targets the total of the user's cart
					userCart.total = (foundProduct.price*prodQuantity) + userCart.total
					userCart.products.push({productId: prodId, quantity: prodQuantity});

					userCart.save()
					.then(save => {
						// console.log(save);
						return response.send('Your cart has been updated. Thank you!')
					})
					.catch(err => {
						console.log(err);
						return response.send('There was an error in updating your cart. Please try again!');
					})
				}
			})
			.catch(err => {
				console.log(err);
				return response.send('Sorry, there was an error in validating the product. Please try again!');
			});

		})
		.catch(err => {
			console.log(err);
			return response.send('Sorry, there was an error locating your cart. Please try again!')
		})
		
	}
};